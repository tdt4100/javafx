package no.hal.javafx.fxmlapp.launch;

import org.eclipse.debug.ui.AbstractLaunchConfigurationTabGroup;
import org.eclipse.debug.ui.CommonTab;
import org.eclipse.debug.ui.EnvironmentTab;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;
import org.eclipse.jdt.debug.ui.launchConfigurations.JavaArgumentsTab;

public class FxmlApplicationTabGroup extends AbstractLaunchConfigurationTabGroup {

	@Override
	public void createTabs(ILaunchConfigurationDialog dialog, String mode) {
//		ILaunchConfiguration configuration = DebugUITools.getLaunchConfiguration(dialog);
//		boolean isModularConfiguration = configuration != null && JavaRuntime.isModularConfiguration(configuration);
		ILaunchConfigurationTab[] tabs = new ILaunchConfigurationTab[] {
			new FxmlTab(),
			new JavaArgumentsTab(),
//			isModularConfiguration ? new JavaDependenciesTab() : new JavaClasspathTab(),
//			new JavaJRETab(),
//			new SourceLookupTab(),
			new EnvironmentTab(),
			new CommonTab()
		};
		setTabs(tabs);
	}
}
