package no.hal.javafx.fxmlapp.launch;

import java.io.File;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jdt.launching.JavaLaunchDelegate;

public class FxmlApplicationLaunchDelegate extends JavaLaunchDelegate {
	
	// Copied from MavenRuntimeClasspathProvider, must be the same!
	// Don't want a hard dependency, so cannot refer directly
	public static final String JDT_JAVA_APPLICATION = "org.eclipse.jdt.launching.localJavaApplication";
	
	public static final String FXML_APPLICATION_LAUNCH_CONFIGURATION_TYPE = "no.hal.javafx.fxmlapp.launch.FxmlApplication";

	public static final String FXML_ATTRIBUTE = FXML_APPLICATION_LAUNCH_CONFIGURATION_TYPE + ".fxmlArg";

	public static String getMainTypeName() {
		return "no.hal.javafx.fxmlapp.lib.FxmlApplication";
	}

	@Override
	public String getProgramArguments(ILaunchConfiguration configuration) throws CoreException {
		String programArguments = super.getProgramArguments(configuration);
		String fxmlArg = getFxmlProgramArguments(configuration);
		return (fxmlArg != null ? programArguments + " " + fxmlArg : programArguments);
	}
	
	static String getFxmlProgramArguments(ILaunchConfiguration configuration) {
		String projectLocation = "", fxmlAttr = "";
		try {
			String projectName = configuration.getAttribute(IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME, "");
			projectLocation = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName).getLocation().toString();
			fxmlAttr = configuration.getAttribute(FXML_ATTRIBUTE, "");
		} catch (CoreException e1) {
		}
		if (projectLocation.length() > 0 && fxmlAttr.length() > 0) {
			String separator = File.separator;
			String fxmlArg = fxmlAttr;
			if ((! fxmlAttr.startsWith(separator)) && (! projectLocation.endsWith(separator))) {
				fxmlArg = separator + fxmlArg;
			}
			return "\"--fxml=" + projectLocation + fxmlArg + "\"";
		}
		return null;
	}
	
	@Override
	public String getVMArguments(ILaunchConfiguration configuration) throws CoreException {
		return super.getVMArguments(configuration);
	}
	
	@Override
	public String getVMArguments(ILaunchConfiguration configuration, String mode) throws CoreException {
		return super.getVMArguments(configuration, mode);
	}
}
